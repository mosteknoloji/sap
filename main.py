import asyncio
import sys
import signal
import robomotion.log as log
import robomotion.plugin as rob
from tendo import singleton
import cb as cb


nFactories = []
handledException = False
namespace = 'com.mosteknoloji.SAP'
loop = None


# https://stackoverflow.com/a/36925722
# CTRL + C hack
async def wakeup():
    while True:
        await asyncio.sleep(1)


def handle_exception(loop, context):
    global handledException
    if not handledException:
        log.info("Plugin shutting down", status="stopped")
        handledException = True


def main():
    me = singleton.SingleInstance()
    classes = sys.modules['__main__'].__dict__['cb'].__dict__
    for c in classes:
        if isinstance(classes[c], type):
            classes[c]().Init()

    global loop
    loop = asyncio.get_event_loop()

    def raise_graceful_exit(*args):
        loop.stop()
        raise SystemExit

    # May want to catch other signals too
    signal.signal(signal.SIGINT, raise_graceful_exit)
    signal.signal(signal.SIGTERM, raise_graceful_exit)
    loop.set_exception_handler(handle_exception)

    try:
        loop.create_task(wakeup())
        loop.create_task(rob.run())
        loop.run_forever()
    finally:
        loop.close()

if __name__ == "__main__":
    main()
