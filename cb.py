import json
import robomotion.messaging as messaging
import robomotion.node as node
import robomotion.runtime as runtime
import sys
import win32com.client
import time
import datetime
from main import namespace

session = None

class Login(node.Node):

    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, credentials=None, client="", variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.credentials = credentials
            self.client = client
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'Login'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['credentials'], config['client'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()
        c = _node.props.credentials

        if c["vaultId"] == "_" or c["itemId"] == "_":
            raise Exception("Missing credentials")

        if _node.props.client == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.client = resp
            else:
                raise Exception("Client may not be empty")

        creds = await runtime.getVaultItem(c["vaultId"], c["itemId"])
        self.login(creds["username"], creds["password"], _node.props.client)

        return json.dumps(msg).encode()

    def on_close_cb(self):
        global session
        session = None
        
        return

    def login(self, username, password, client):
        session.findById("wnd[0]/usr/txtRSYST-MANDT").text = client
        session.findById("wnd[0]/usr/txtRSYST-BNAME").text = username
        session.findById("wnd[0]/usr/pwdRSYST-BCODE").text = password
        session.findById("wnd[0]").sendVKey(0)


class StartTransaction(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, variable=None, transaction=''):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.transaction = transaction
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'StartTransaction'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['variable'], config['transaction'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.transaction == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.transaction = resp
            else:
                raise Exception("Transaction may not be empty")

        session.StartTransaction(_node.props.transaction)
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class SelectElement(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'Select'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        try:
            session.findById(_node.props.element_id).select()
        except:
            pass
        session.findById(_node.props.element_id).setFocus()

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class GetText(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'GetText'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        msg['text'] = session.findById(_node.props.element_id).text

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class SetText(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', text='', variable1=None,
                     variable2=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.text = text
            self.variable1 = variable1
            self.variable2 = variable2

    def __init__(self):
        self.node_name = namespace+'.'+'SetText'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['text'], config['variable1'], config['variable2'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable1
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        variable = _node.props.variable2
        if _node.props.text == '' and not variable['scope'] == '_' \
                and not variable['name'] == '_' and not variable['name'] == '':
            resp = await get_variable(variable, msg)
            if resp is None:
                raise Exception("Variable could not be found")

            _node.props.text = resp

        session.findById(_node.props.element_id).text = _node.props.text

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class ClickElement(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'Click'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        session.findById(_node.props.element_id).press()
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class GetCheckbox(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'GetCheckbox'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        if session.findById(_node.props.element_id).selected == 0:
            msg['state'] = 'unchecked'
        elif session.findById(_node.props.element_id).selected == -1:
            msg['state'] = 'checked'
        else:
            msg['state'] = 'unknown'

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class SetCheckbox(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, variable1=None, element_id='',
                     variable2=None, action=''):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.variable1 = variable1
            self.element_id = element_id
            self.variable2 = variable2
            self.action = action

    def __init__(self):
        self.node_name = namespace+'.'+'SetCheckbox'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['variable1'], config['elementId'], config['variable2'], config['action'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable1
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        if _node.props.action == "_":
            variable = _node.props.variable2
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.action = resp
            else:
                raise Exception("Action may not be empty")

        if _node.props.action == "checked":
            session.findById(_node.props.element_id).selected = -1
        elif _node.props.action == "unchecked":
            session.findById(_node.props.element_id).selected = 0
        elif _node.props.action == "toggle":
            state = session.findById(_node.props.element_id).selected
            session.findById(_node.props.element_id).selected = ((state+2) % 2)-1

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class GoBack(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)

    def __init__(self):
        self.node_name = namespace+'.'+'Back'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()
        session.findById('wnd[0]').sendVKey(3)  # Send F3
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class Cancel(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)

    def __init__(self):
        self.node_name = namespace+'.'+'Cancel'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()
        session.findById('wnd[0]').sendVKey(12)  # Send F12
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class Logout(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)

    def __init__(self):
        self.node_name = namespace+'.'+'Logout'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        parent = session.Parent
        while len(parent.Children) > 0:
            _session = parent.Children(0)
            while _session.findById("wnd[0]/tbar[0]/btn[3]").changeable:  # Can go back
                _session.findById("wnd[0]").sendVKey(3)  # Send F3

            _session.findById("wnd[0]").sendVKey(15)  # Send Shift + F3
            if len(_session.Children) == 2:
                _session.findById("wnd[1]/usr/btnSPOP-OPTION1").press()  # Press yes

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class IsEnabled(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'IsEnabled'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        is_enabled = session.findById(_node.props.element_id).changeable
        if is_enabled:
            msg['state'] = 'enabled'
        else:
            msg['state'] = 'disabled'

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class Screenshot(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, variable1=None, element_id='',
                     variable2=None, path=''):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.variable1 = variable1
            self.element_id = element_id
            self.variable2 = variable2
            self.path = path

    def __init__(self):
        self.node_name = namespace+'.'+'Screenshot'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['variable1'], config['elementId'], config['variable2'], config['path'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable1
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        if _node.props.path == "":
            variable = _node.props.variable2
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.path = resp
            else:
                raise Exception("Path may not be empty")

        session.findById(_node.props.element_id).HardCopy(_node.props.path, 1)
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class RunScript(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, script=''):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.script = script

    def __init__(self):
        self.node_name = namespace+'.'+'RunScript'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['func'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        def can_execute():
            lines = _node.props.script.split('\n')
            for line in lines:
                if not line.startswith('session.findById') and not line.startswith('msg['):
                    return False
            return True

        if can_execute():
            exec(_node.props.script)
        else:
            raise Exception('Script is not safe to execute')

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class ScrapeTable(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'Scrape'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        tbl = {
            'cols': [],
            'rows': []
        }

        table = session.findById(_node.props.element_id)
        for col in table.Columns:
            if col.Title != '':
                tbl['cols'].append(col.Title)
            elif col.IconName != '':
                tbl['cols'].append(col.IconName)

        col_size = len(table.Columns)
        page = 0
        offset = 0
        for i in range(table.RowCount):
            if i % table.VisibleRowCount == 0:
                table.verticalScrollbar.position = table.VisibleRowCount * page
                table = session.findById(_node.props.element_id)
                offset += row_count(table, page)
                page += 1
            if i == offset:
                break

            cells = []
            row = table.GetAbsoluteRow(i)
            for j in range(col_size):
                el_type = row.ElementAt(j).Type
                if el_type == 'GuiTextField':
                    cells.append(row.ElementAt(j).Text)
                if el_type == 'GuiCheckBox':
                    cells.append(str(row.ElementAt(j).Selected))

            tbl['rows'].append(cells)
        msg['table'] = tbl
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class SetTable(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'SetTable'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable1
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        cols = []
        rows = []
        variable = _node.props.variable2
        if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
            resp = await get_variable(variable, msg)
            if resp is None:
                raise Exception("Variable could not be found")

            def set_table():
                nonlocal cols
                nonlocal rows
                cols = resp['cols']
                rows = resp['rows']

            set_table()
        else:
            raise Exception("Table could not be found")

        table = session.findById(_node.props.element_id)

        c = 0
        for col in cols:
            table.Columns(c).title = col
            c += 1

        i = 0
        for row in rows:
            _row = table.GetAbsoluteRow(i)
            i += 1
            j = 0
            for element in row:
                _cell = _row.ElementAt(j)
                _cell.text = element
                j += 1

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class GetTrafficLight(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'TrafficLight'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        icon = session.findById(_node.props.element_id).IconName
        if icon == 'S_TL_G':
            msg['state'] = 'green'
        elif icon == 'S_TL_Y':
            msg['state'] = 'yellow'
        elif icon == 'S_TL_R':
            msg['state'] = 'red'
        else:
            msg['state'] = 'unknown'
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class CloseWindow(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'Close'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        if _node.props.element_id == 'wnd[0]':
            global session
            _connection = session.Parent
            session.findById(_node.props.element_id).Close()
            session = _connection.Children(0)
        else:
            session.findById(_node.props.element_id).Close()

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class GetCombobox(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'GetCombobox'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        cmb = session.findById(_node.props.element_id)
        for entry in cmb.Entries:
            if entry.Key == cmb.Key:
                msg['index'] = entry.Pos-1
                break
        msg['text'] = cmb.Key
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class SetCombobox(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', value='', variable1=None,
                     variable2=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.value = value
            self.variable1 = variable1
            self.variable2 = variable2

    def __init__(self):
        self.node_name = namespace+'.'+'SetCombobox'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['value'], config['variable1'], config['variable2'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable1
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        if _node.props.value == "":
            variable = _node.props.variable2
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.value = resp
            else:
                raise Exception("Value may not be empty")

        cmb = session.findById(_node.props.element_id)
        cmb.SetFocus()
        for entry in cmb.Entries:
            if entry.Value == _node.props.value or entry.key == _node.props.value:
                cmb.key = entry.key
                break
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class GetTransactions(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, exclude_favs=True):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.exclude_favs = exclude_favs

    def __init__(self):
        self.node_name = namespace+'.'+'GetTrx'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['excludeFavs'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        shell = session.findById('wnd[0]/usr/cntlIMAGE_CONTAINER/shellcont/shell/shellcont[0]/shell')
        nodes = []
        favs_excluded = False
        for key in shell.GetAllNodeKeys():
            if shell.IsFolder(key):
                if not favs_excluded and _node.props.exclude_favs:
                    if shell.GetNodeTextByKey(key) != "Favorites":
                        favs_excluded = True
            elif favs_excluded or not _node.props.exclude_favs:
                name = shell.GetNodeTextByKey(key)
                name_parts = name.split(' - ')
                nodes.append({
                    'code': name_parts[0],
                    'name': name_parts[1]
                })

        msg['trx'] = nodes
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class PossibleEntries(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'PossibleEntries'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == "":
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception("Element ID may not be empty")

        session.findById(_node.props.element_id).setFocus()
        wnd = _node.props.element_id.split('/')[0]
        session.findById(wnd).sendVKey(4)

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class NewSession(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)

    def __init__(self):
        self.node_name = namespace+'.'+'NewSession'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        global session

        _sessions = session.Parent.Children
        count = len(_sessions)
        session.findById("wnd[0]").sendVKey(74)

        while len(_sessions) == count:
            _sessions = session.Parent.Children

        session = _sessions(len(_sessions)-1)
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class SessionCount(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)

    def __init__(self):
        self.node_name = namespace+'.'+'SessionCount'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()
        msg['count'] = len(session.Parent.Children)
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class WaitElement(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id='', timeout=0, variable1=None,
                     variable2=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.timeout = timeout
            self.variable1 = variable1
            self.variable2 = variable2

    def __init__(self):
        self.node_name = namespace+'.'+'Wait'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['timeout'], config['variable1'], config['variable2'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == '':
            variable = _node.props.variable1
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception('Element ID may not be empty')

        variable = _node.props.variable2
        if node.props.timeout == 0 and not variable['scope'] == '_' \
                and not variable['name'] == '_' and not variable['name'] == '':
            resp = await get_variable(variable, msg)
            if resp is None:
                raise Exception("Variable could not be found")

            _node.props.timeout = resp

        msg['waited'] = 0
        start = datetime.datetime.now()
        while (datetime.datetime.now() - start).seconds < _node.props.timeout:
            try:
                session.findById(_node.props.element_id)
                msg['waited'] = 1
                break
            except:
                time.sleep(0.1)
                get_session()
                pass
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class SendKey(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, element_id=0, key='', mod1='', mod2='',
                     mod3='', variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.element_id = element_id
            self.key = key
            self.mod1 = mod1
            self.mod2 = mod2
            self.mod3 = mod3
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'SendKey'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['elementId'], config['key'], config['mod1'], config['mod2'], config['mod3'],
                                     config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)
        get_session()

        if _node.props.element_id == '':
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.element_id = resp
            else:
                raise Exception('Element Id may not be empty')

        if _node.props.key == '':
            total = 0
            can_send = 0
            modifiers = [_node.props.mod1, _node.props.mod2, _node.props.mod3]

            def get_vkey(modifiers):
                for modifier in modifiers:
                    if modifier != '_':
                        nonlocal can_send
                        nonlocal total
                        if modifier[0] == '+':
                            modifier = modifier[1:]
                        total += int(modifier)
                        can_send = 1

            get_vkey(modifiers)
            if can_send == 1:
                session.findById(_node.props.element_id).sendVKey(total)
        elif _node.props.mod1 == '+24' and _node.props.mod2 == '_' and _node.props.mod3 == '_':  # ctrl + key
            if _node.props.key.lower() == 'f':
                session.findById(_node.props.element_id).sendVKey(71)
            elif _node.props.key.lower() == 'n':
                session.findById(_node.props.element_id).sendVKey(74)
            elif _node.props.key.lower() == 'p':
                session.findById(_node.props.element_id).sendVKey(86)
            elif _node.props.key.lower() == 's':
                session.findById(_node.props.element_id).sendVKey(11)

        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


class OpenConnection(node.Node):
    class NodeProps(node.INodeProps):
        def __init__(self, guid='', name='', delay_before=0, delay_after=0, description=0, variable=None):
            super(type(self), self).__init__(guid, name, delay_before, delay_after)
            self.description = description
            self.variable = variable

    def __init__(self):
        self.node_name = namespace+'.'+'Connect'
        self.props = self.NodeProps()

    def Init(self):
        node.NodeFactory(self, self.node_name)
        return

    def on_create_cb(self, _node, config):
        _node.props = self.NodeProps(config['guid'], config['name'], config['delayBefore'], config['delayAfter'],
                                     config['description'], config['variable'])
        return

    async def on_message_cb(self, _node, data):
        msg = json.loads(data)

        if _node.props.description == '':
            variable = _node.props.variable
            if not variable['scope'] == '_' and not variable['name'] == '_' and not variable['name'] == '':
                resp = await get_variable(variable, msg)
                if resp is None:
                    raise Exception("Variable could not be found")

                _node.props.description = resp
            else:
                raise Exception('Description may not be empty')

        SapGuiAuto = win32com.client.GetObject("SAPGUI")
        if not isinstance(SapGuiAuto, win32com.client.CDispatch):
            return json.dumps(msg).encode()

        application = SapGuiAuto.GetScriptingEngine
        if not isinstance(application, win32com.client.CDispatch):
            SapGuiAuto = None
            return json.dumps(msg).encode()

        application.OpenConnection(_node.props.description)
        return json.dumps(msg).encode()

    def on_close_cb(self):
        return


def row_count(table, page):
    c = 0
    try:
        for i in range(table.VisibleRowCount):
            table.GetAbsoluteRow(table.VisibleRowCount*page + i).ElementAt(0)
            c += 1
    finally:
        return c


def get_session():
    try:
        global session

        SapGuiAuto = win32com.client.GetObject("SAPGUI")
        if not isinstance(SapGuiAuto, win32com.client.CDispatch):
            return

        application = SapGuiAuto.GetScriptingEngine
        if not isinstance(application, win32com.client.CDispatch):
            SapGuiAuto = None
            return

        session = application.ActiveSession
        if not isinstance(session, win32com.client.CDispatch):
            connection = application.Children(0)
            if not isinstance(connection, win32com.client.CDispatch):
                application = None
                SapGuiAuto = None
                return

            session = connection.Children(0)
            if not isinstance(session, win32com.client.CDispatch):
                session = None
                connection = None
                application = None
                SapGuiAuto = None
                return
    except:
        print(sys.exc_info()[0])
    finally:
        connection = None
        application = None
        SapGuiAuto = None


async def get_variable(variable, msg):
    scope = variable['scope']
    name = variable['name']
    if scope == 'Message':
        if name in msg:
            return msg[name]
        else:
            return None
    else:
        data = json.dumps(variable).encode()
        resp = await messaging.request('GetVariable', data)
        resp_data = json.loads(resp.data.decode())
        if 'value' in resp_data:
            return resp_data['value']
        else:
            return None
